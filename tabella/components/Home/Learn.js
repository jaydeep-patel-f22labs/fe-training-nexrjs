const Learn = () => {
  return (
    <div className="p-4 bg-theme-grey mt-9 lg:px-32 lg:mb-28">
      <div className="mt-8 flex justify-center items-center flex-col lg:flex-row">
        <img
          src="img/group-10-3-x.jpg"
          srcSet="img/group-10-3-x@2x.jpg 2x,
             img/group-10-3-x@3x.jpg 3x"
            alt='imglearn'
            className='w-48 h-auto lg:w-full object-contain lg:mr-36'
        />
        <div className="text-center lg:text-left">
          <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5">
            Learn Anywhere
          </p>
          <p className="text-xs mb-3 lg:text-xl">
            Make use of the technology available and access Tabella everywhere,
            be it your home or inside the Mariana Trench. It is available on
            both Android and iOS platforms.
          </p>
          <div className="flex justify-center lg:justify-start">
          <div className="">
            <img
              src="img/google-play-badge-3-x.jpg"
              srcSet="img/google-play-badge-3-x@2x.jpg 2x,
             img/google-play-badge-3-x@3x.jpg 3x"
             alt='imgLearn1'
             className='w-24 my-3.5 mr-3.5'
            />
          </div>
          <div className="ml-2">
            <img
              src="img/google-play-badge-3-x.jpg"
              srcSet="img/google-play-badge-3-x@2x.jpg 2x,
             img/google-play-badge-3-x@3x.jpg 3x"
             alt='googlePlay'
             className='w-24 my-3.5 mr-3.5'
            />
          </div>
        </div>
        </div>
       
      </div>
    </div>
  );
};

export default Learn;

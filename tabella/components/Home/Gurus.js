const Gurus = () => {
  return (
    <div className=" -mt-16 inline-block">
      <div className="bg-theme-grey lg:py-20">
        <div className="p-4 lg:px-32 ">
          <div className="text-center ">
            <p className="mt-1 mb-3 font-bold text-xs lg:text-4xl lg:mb-5">
              Let us introduce you to our Gurus
            </p>
            <p className="text-xs mb-3 lg:text-xl">
              Our teachers at Tabella are highly experienced and their passion
              to mentor and inculcate their padawans with their maximal
              knowledge is mind blowing.
            </p>
          </div>
          <div className="flex justify-center">
            <div className="relative ">
              <img
                src="/img/34767.svg"
                alt="img1"
                className="h-20 w-28 ml-5 mb-6 lg:w-72 lg:h-40 lg:mb-5"
              />
              <div className="absolute top-0 right-0 text-right lg:hidden">
                <p className="font-bold text-sm">Teng Jiang</p>
                <p className='text-1/2 w-24'>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
            <div>
              <img
                src="/img/34767.svg"
                alt="img2"
                className="h-20 w-28 ml-5 mb-6 lg:w-72 lg:h-40 lg:mb-5"
              />
            </div>
          </div>
          <div className="flex justify-center">
            <div>
              <img
                src="/img/34767.svg"
                alt="img3"
                className="h-20 w-28 ml-5 mb-6 lg:w-72 lg:h-40 lg:mb-5"
              />
            </div>
            <div>
              <img
                src="/img/34767.svg"
                alt="img4"
                className="h-20 w-28 ml-5 mb-6 lg:w-72 lg:h-40 lg:mb-5"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Gurus;

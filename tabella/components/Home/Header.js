import styles from "../../styles/Home.module.css";

const Header = () => {
  return (
    <div className="relative mx-4 mt-2 md:mx-20 lg:mt-0">
      <div className="absolute left-0 top-0 lg:hidden">
        <div className='mt-3'>
        <div className={styles.Path}></div>
        <div className={styles.Path}></div>
        <div className={styles.Path}></div>
        </div>
      </div>
      <div className="absolute left-1/3 ml-5 top-0 lg:left-0 lg:ml-0 lg:mt-7">
        <img src="/img/group-4.svg" alt='img5' className='h-auto w-auto lg:w-28' />
      </div>
     
    </div>
  );
};

export default Header;

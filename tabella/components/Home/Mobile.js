const Mobile = () => {
  return (
    <div className="flex justify-center items-center flex-col m-4 lg:mx-32">
      <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5 lg:mt-16">Why Learn on Tabella</p>
      <p className="text-center text-xs mb-3 lg:text-xl">
        Tabella follows a comprehensive method of learn, practice and test for
        deep immersive learning. It uses various tools like animations, videos
        and virtual labs to make learning easy and engaging. Our courses are
        designed to be entertaining, challenging, and educational. They are for
        ambitious students, professionals, and lifelong learners.
      </p>
      <div className='hidden justify-center my-7 lg:flex'>
          <div className='h-3 w-3 bg-black rounded-full mr-3.5'></div>
          <div className='h-3 w-3 bg-gray-400 rounded-full mr-3.5'></div>
          <div className='h-3 w-3 bg-gray-400 rounded-full mr-3.5'></div>
          <div className='h-3 w-3 bg-gray-400 rounded-full mr-3.5'></div>
      </div>
      <img
        className="h-auto w-auto mt-6 lg:mt-3 lg:w-72 lg:h-auto"
        src="img/aaaaaaaaa-3-x.jpg"
        alt='img8'
        
      />
    </div>
  );
};

export default Mobile;
